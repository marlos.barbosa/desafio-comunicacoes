import { writeFileSync } from "fs"
import fetch from "node-fetch"
import download from "download"

const imageSearch = async(key)=>{
    const url = `https://list.ly/api/v4/search/image?q=${key}`
    const response = await fetch(url)
    const information = await response.json()
    writeFileSync("searchResult.json",await download(url))
    const results = information.results
    for (let i=0;i<results.length;i++){
        const saveImage = async(data,index) =>{
            writeFileSync(`${index}image.jpg`,await download(data[index].image))
        }
        saveImage(results,i)
    }
}
imageSearch("cars")